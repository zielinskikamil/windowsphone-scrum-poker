using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using ScrumPoker.Annotations;
using ScrumPoker.Models;

public class MyDeckViewModel : INotifyPropertyChanged
{
	public String UniqueId { get; set; }
	public String Value { get; set; }
	public String Name { get; set; }
	public ObservableCollection<Card> Cards { get; set; }

	public event PropertyChangedEventHandler PropertyChanged;

	public MyDeckViewModel()
	{
		Cards = new ObservableCollection<Card>();
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	{
		PropertyChangedEventHandler handler = PropertyChanged;
		if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
	}

	public void AddCard(Card card)
	{
		var isAlreadyExists = Cards.Any(c => c.Value == card.Value);
		if (isAlreadyExists) return;
		Cards.Add(card);
	}
}