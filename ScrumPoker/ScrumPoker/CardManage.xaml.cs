﻿using ScrumPoker.Common;
using ScrumPoker.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ScrumPoker.ViewModels;

namespace ScrumPoker
{
	public sealed partial class CardManage : Page
	{
		private readonly NavigationHelper _navigationHelper;
		public NavigationHelper NavigationHelper
		{
			get { return this._navigationHelper; }
		}

		private CreateDeckViewModel _createDeckViewModel;
		public CreateDeckViewModel CreateDeckViewModel
		{
			get { return _createDeckViewModel ?? (_createDeckViewModel = new CreateDeckViewModel()); }
		}

		public CardManage()
		{
			this.InitializeComponent();

			this._navigationHelper = new NavigationHelper(this);
			this._navigationHelper.LoadState += this.NavigationHelper_LoadState;
			this._navigationHelper.SaveState += this.NavigationHelper_SaveState;
		}

		private void DeckGridView_OnItemClick(object sender, ItemClickEventArgs e)
		{
			var clickedCard = e.ClickedItem as Card;
			CreateDeckViewModel.RemoveCard(clickedCard);
		}

		private async void CreateNewDeck_Click(object sender, RoutedEventArgs e)
		{
			await App.DataSource.AddDeckAsync(CreateDeckViewModel.NewDeck);
			Frame.Navigate(typeof(HubPage));
		}

		#region NavigationHelper registration

		/// <summary>
		/// Populates the page with content passed during navigation.  Any saved state is also
		/// provided when recreating a page from a prior session.
		/// </summary>
		/// <param name="sender">
		/// The source of the event; typically <see cref="NavigationHelper"/>
		/// </param>
		/// <param name="e">Event data that provides both the navigation parameter passed to
		/// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
		/// a dictionary of state preserved by this page during an earlier
		/// session.  The state will be null the first time a page is visited.</param>
		private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
		{
		}

		/// <summary>
		/// Preserves state associated with this page in case the application is suspended or the
		/// page is discarded from the navigation cache.  Values must conform to the serialization
		/// requirements of <see cref="SuspensionManager.SessionState"/>.
		/// </summary>
		/// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
		/// <param name="e">Event data that provides an empty dictionary to be populated with
		/// serializable state.</param>
		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
		{
		}

		/// <summary>
		/// The methods provided in this section are simply used to allow
		/// NavigationHelper to respond to the page's navigation methods.
		/// <para>
		/// Page specific logic should be placed in event handlers for the  
		/// <see cref="NavigationHelper.LoadState"/>
		/// and <see cref="NavigationHelper.SaveState"/>.
		/// The navigation parameter is available in the LoadState method 
		/// in addition to page state preserved during an earlier session.
		/// </para>
		/// </summary>
		/// <param name="e">Provides data for navigation methods and event
		/// handlers that cannot cancel the navigation request.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedFrom(e);
		}

		#endregion
	}
}
