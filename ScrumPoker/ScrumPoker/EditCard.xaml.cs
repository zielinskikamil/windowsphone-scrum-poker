﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ScrumPoker.Common;
using ScrumPoker.Models;
using ScrumPoker.ViewModels;

namespace ScrumPoker
{
	public sealed partial class EditCard : Page
	{
		private readonly NavigationHelper _navigationHelper;
		public NavigationHelper NavigationHelper
		{
			get { return this._navigationHelper; }
		}

		private EditCardViewModel _editCardViewModel;
		public EditCardViewModel EditCardVM
		{
			get { return _editCardViewModel ?? (_editCardViewModel = new EditCardViewModel()); }
		}

		public EditCard()
		{
			InitializeComponent();
			_navigationHelper = new NavigationHelper(this);
			_navigationHelper.LoadState += NavigationHelper_LoadState;
			_navigationHelper.SaveState += NavigationHelper_SaveState;
		}

		private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
		{
			EditCardVM.NewDeck = App.DataSource.GetDeckByUniqueIdAsync((string)e.NavigationParameter);
		}

		private void DeckGridView_OnItemClick(object sender, ItemClickEventArgs e)
		{
			Card clickedCard = e.ClickedItem as Card;
			EditCardVM.RemoveCard(clickedCard);
		}

		private async void SaveChanges_Click(object sender, RoutedEventArgs e)
		{
			await App.DataSource.EditDeckAsync(EditCardVM.NewDeck);
			Frame.Navigate(typeof(HubPage));
		}

		#region NavigationHelper registration

		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
		{
		}

		/// <summary>
		/// The methods provided in this section are simply used to allow
		/// NavigationHelper to respond to the page's navigation methods.
		/// <para>
		/// Page specific logic should be placed in event handlers for the  
		/// <see cref="NavigationHelper.LoadState"/>
		/// and <see cref="NavigationHelper.SaveState"/>.
		/// The navigation parameter is available in the LoadState method 
		/// in addition to page state preserved during an earlier session.
		/// </para>
		/// </summary>
		/// <param name="e">Provides data for navigation methods and event
		/// handlers that cannot cancel the navigation request.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedFrom(e);
		}

		#endregion
	}
}
