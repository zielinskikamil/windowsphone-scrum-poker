﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ScrumPoker.Models;

namespace ScrumPoker.DataModel
{
	public sealed class DataSource
	{
		private readonly DataLoader _dataLoader = new DataLoader();
		private readonly ObservableCollection<Deck> _decks = new ObservableCollection<Deck>();
		public ObservableCollection<Deck> Decks
		{
			get { return _decks; }
		}

		public DataSource()
		{
			_decks = _dataLoader.GetDecks().Result;
		}

		public async Task EditDeckAsync(Deck deck)
		{
			Deck deckToEdit = _decks.FirstOrDefault(d=>d.UniqueId == deck.UniqueId);

			deckToEdit.Name = deck.Name;
			deckToEdit.Value = deck.Value;
			deckToEdit.Cards = deck.Cards;

			await _dataLoader.SaveChangesAsync(_decks);
		}

		public async Task SaveChanges()
		{
			await _dataLoader.SaveChangesAsync(_decks);
		}

		public async Task AddDeckAsync(Deck deck)
		{
			if (_decks.Count != 0)
				_decks.Add(deck);
			await _dataLoader.SaveChangesAsync(_decks);
		}

		public Deck GetDeckByUniqueIdAsync(string uniqueId)
		{
			var matches = Decks.Where((group) => group.UniqueId.Equals(uniqueId));
			if (matches.Count() == 1) return matches.First();
			return null;
		}

		public Card GetCardByIdAsync(string uniqueId)
		{
			var matches = Decks.SelectMany(group => group.Cards).Where((item) => item.Id.Equals(uniqueId));
			if (matches.Any()) return matches.First();
			return null;
		}
	}
}