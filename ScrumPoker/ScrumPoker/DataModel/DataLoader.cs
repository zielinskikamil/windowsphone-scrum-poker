﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using ScrumPoker.Models;

namespace ScrumPoker.DataModel
{
	public class DataLoader
	{
		private const String FileName = "decks.json";

		private async Task<ObservableCollection<Deck>> GetDateAsync()
		{
			ObservableCollection<Deck> _decks = new ObservableCollection<Deck>();

			if (_decks.Count() != 0)
				return _decks;

			var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Deck>));

			try
			{
				using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(FileName))
				{
					_decks = (ObservableCollection<Deck>)jsonSerializer.ReadObject(stream);
				}

			}
			catch
			{
				_decks = GetSampleDataAsync().Result;
			}
			return _decks;
		}

		public async Task SaveChangesAsync(ObservableCollection<Deck> decks)
		{
			var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<Deck>));
			using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(FileName,
				CreationCollisionOption.ReplaceExisting))
			{
				jsonSerializer.WriteObject(stream, decks);
			}
		}

		public async Task<ObservableCollection<Deck>> GetDecks()
		{
			return await GetDateAsync();
		}

		private async Task<ObservableCollection<Deck>> GetSampleDataAsync()
		{
			ObservableCollection<Deck> _decks = new ObservableCollection<Deck>();

			Uri dataUri = new Uri("ms-appx:///DataModel/SampleData.json");

			StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
			string jsonText = await FileIO.ReadTextAsync(file);
			JsonObject jsonObject = JsonObject.Parse(jsonText);
			JsonArray jsonArray = jsonObject["Decks"].GetArray();

			foreach (JsonValue groupValue in jsonArray)
			{
				JsonObject groupObject = groupValue.GetObject();
				Deck deck = new Deck()
				{
					UniqueId = groupObject["UniqueId"].GetString(),
					Name = groupObject["Name"].GetString(),
					ImagePath = groupObject["ImagePath"].GetString()
				};

				deck.Cards = new ObservableCollection<Card>();

				foreach (JsonValue itemValue in groupObject["Cards"].GetArray())
				{
					JsonObject cardObject = itemValue.GetObject();
					deck.Cards.Add(new Card()
					{
						Id = cardObject["Id"].GetString(),
						Value = cardObject["Value"].GetString(),
						ImagePath = cardObject["ImagePath"].GetString()
					});

				}
				_decks.Add(deck);
			}

			return _decks;
		}
	}
}
