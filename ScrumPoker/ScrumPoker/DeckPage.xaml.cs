﻿using ScrumPoker.Common;
using ScrumPoker.Models;
using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace ScrumPoker
{
	public sealed partial class DeckPage : Page
	{
		private readonly NavigationHelper _navigationHelper;
		private readonly ObservableDictionary _defaultViewModel = new ObservableDictionary();
		public Deck Deck { get; set; }

		public DeckPage()
		{
			InitializeComponent();

			_navigationHelper = new NavigationHelper(this);
			_navigationHelper.LoadState += NavigationHelper_LoadState;
			_navigationHelper.SaveState += NavigationHelper_SaveState;
		}

		public NavigationHelper NavigationHelper
		{
			get { return _navigationHelper; }
		}

		public ObservableDictionary DefaultViewModel
		{
			get { return _defaultViewModel; }
		}


		private  void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
		{
			var Deck =  App.DataSource.GetDeckByUniqueIdAsync((string)e.NavigationParameter);
			DefaultViewModel["Deck"] = Deck;
		}

		private void EditDeck_Click(object sender, RoutedEventArgs e)
		{
			Deck deck = (Deck)DefaultViewModel["Deck"];
			Frame.Navigate(typeof(EditCard), deck.UniqueId);
		}

		private async void DeleteDeck_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new MessageDialog("Are you sure?");
			dialog.Title = "Delete deck";
			dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
			dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
			var res = await dialog.ShowAsync();

			if ((int)res.Id == 0)
			{
				App.DataSource.Decks.Remove((Deck)DefaultViewModel["Deck"]);
				Frame.Navigate(typeof(HubPage));
			}
		}
		
		/// <summary>
		/// Shows the details of an item clicked on in the <see cref="ItemPage"/>
		/// </summary>
		/// <param name="sender">The GridView displaying the item clicked.</param>
		/// <param name="e">Event data that describes the item clicked.</param>
		private void ShowCard_ItemClick(object sender, ItemClickEventArgs e)
		{
			var itemId = ((Card)e.ClickedItem).Id;
			if (!Frame.Navigate(typeof(ItemPage), itemId))
			{
				var resourceLoader = ResourceLoader.GetForCurrentView("Resources");
				throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
			}
		}

		#region NavigationHelper registration

		/// <summary>
		/// The methods provided in this section are simply used to allow
		/// NavigationHelper to respond to the page's navigation methods.
		/// <para>
		/// Page specific logic should be placed in event handlers for the
		/// <see cref="NavigationHelper.LoadState"/>
		/// and <see cref="NavigationHelper.SaveState"/>.
		/// The navigation parameter is available in the LoadState method
		/// in addition to page state preserved during an earlier session.
		/// </para>
		/// </summary>
		/// <param name="e">Event data that describes how this page was reached.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			_navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			_navigationHelper.OnNavigatedFrom(e);
		}

		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) { }

		#endregion
	}
}
