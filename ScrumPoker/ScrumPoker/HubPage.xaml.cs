﻿using System.Collections.ObjectModel;
using ScrumPoker.Common;
using ScrumPoker.Models;
using System;
using System.Linq;
using Windows.ApplicationModel.Resources;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ScrumPoker.ViewModels;

namespace ScrumPoker
{
	public sealed partial class HubPage : Page
	{
		private readonly NavigationHelper _navigationHelper;
		public NavigationHelper NavigationHelper
		{
			get { return _navigationHelper; }
		}

		private HomePageViewModel _homePageViewModel;
		public HomePageViewModel HomePageViewModel
		{
			get { return _homePageViewModel ?? (_homePageViewModel = new HomePageViewModel()); }
		}

		private readonly ResourceLoader _resourceLoader = ResourceLoader.GetForCurrentView("Resources");

		public HubPage()
		{
			InitializeComponent();

			DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

			NavigationCacheMode = NavigationCacheMode.Required;

			_navigationHelper = new NavigationHelper(this);
			_navigationHelper.LoadState += NavigationHelper_LoadState;
			_navigationHelper.SaveState += NavigationHelper_SaveState;
		}

		/// <summary>
		/// Shows the chosen deck
		/// </summary>
		private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
		{
			var itemId = ((Deck)e.ClickedItem).UniqueId;
			if (!Frame.Navigate(typeof(DeckPage), itemId))
			{
				throw new Exception(this._resourceLoader.GetString("NavigationFailedExceptionMessage"));
			}
		}

		private void AddNewDeck_Click(object sender, RoutedEventArgs e)
		{
			Frame.Navigate(typeof(CardManage));
		}

		#region NavigationHelper registration

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			this._navigationHelper.OnNavigatedFrom(e);
		}

		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e){}

		private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
		{
			HomePageViewModel.Decks = App.DataSource.Decks;
		}

		#endregion
	}
}
