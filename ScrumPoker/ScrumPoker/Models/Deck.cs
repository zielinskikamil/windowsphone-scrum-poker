﻿using System;
using System.Collections.ObjectModel;
using ScrumPoker.Common;

namespace ScrumPoker.Models
{
	public class Deck : ObservableObject
	{
		#region Fields

		private string _uniqueId;
		private string _value;
		private string _name;
		private string _imagePath;

		#endregion

		#region Properties

		public String UniqueId
		{
			get { return _uniqueId; }
			set
			{
				if (value != _uniqueId)
				{
					_uniqueId = value;
					OnPropertyChanged("UniqueId");
				}
			}
		}

		public String Value
		{
			get { return _value; }
			set
			{
				if (value != _value)
				{
					_value = value;
					OnPropertyChanged("Value");
				}
			}
		}

		public String Name
		{
			get { return _name; }
			set
			{
				if (value != _name)
				{
					_name = value;
					OnPropertyChanged("Name");
				}
			}
		}

		public String ImagePath
		{
			get { return _imagePath; }
			set
			{
				if (value != _imagePath)
				{
					_imagePath = value;
					OnPropertyChanged("ImagePath");
				}
			}
		}

		public ObservableCollection<Card> Cards { get; set; }

		#endregion
	}
}
