﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScrumPoker.Common;

namespace ScrumPoker.Models
{
	public class Card : ObservableObject
	{
		#region Fields

		private string _id;
		private string _value;
		private string _imagePath;

		#endregion

		#region Properties

		public string Id
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					OnPropertyChanged("Id");
				}
			}
		}

		public string Value
		{
			get { return _value; }
			set
			{
				if (value != _value)
				{
					_value = value;
					OnPropertyChanged("Value");
				}
			}
		}

		public string ImagePath
		{
			get { return _imagePath; }
			set
			{
				if (value != _imagePath)
				{
					_imagePath = value;
					OnPropertyChanged("ImagePath");
				}
			}
		}

		#endregion
	}
}
