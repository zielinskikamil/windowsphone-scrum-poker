﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace ScrumPoker.Converters
{
	public class ImagePathToChosenThemeConventer : IValueConverter
	{
		private readonly Color _darkthemeColor = Color.FromArgb(255, 0, 0, 0);

		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if (value == null) return "";

			string imagePath = value.ToString();

			if (IsDarkTheme() || String.IsNullOrEmpty(imagePath))
				return value;

			return imagePath.Replace("dark", "light");
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return value;
		}

		private bool IsDarkTheme()
		{
			var backBrush = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;
			return backBrush.Color == _darkthemeColor;
		}
	}
}
