﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ScrumPoker.Converters
{
	public class CardFontConventer : DependencyObject, IValueConverter
	{
		public string TextProp
		{
			get { return (string)GetValue(CurrentUserProperty); }
			set { SetValue(CurrentUserProperty, value); }
		}

		public static readonly DependencyProperty CurrentUserProperty =
	   DependencyProperty.Register("TextProp",
								   typeof(string),
								   typeof(CardFontConventer),
								   new PropertyMetadata(null));

		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if (TextProp == null) return 1;
			int fontSize = (int)((double)value / (TextProp.Length + 1));
			return fontSize;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return value;
		}
	}
}
