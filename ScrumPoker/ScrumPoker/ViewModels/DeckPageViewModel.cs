﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ScrumPoker.Common;
using ScrumPoker.Models;

namespace ScrumPoker.ViewModels
{
	public class DeckPageViewModel
	{
		public Deck Deck { get; set; }

		public ICommand _deleteDeckCommand;
		public ICommand DeleteDeckCommand
		{
			get { return _deleteDeckCommand ?? (_deleteDeckCommand = new RelayCommand(DeleteDeck)); }
		}

		private void DeleteDeck()
		{
			throw new NotImplementedException();
		}
	}
}
