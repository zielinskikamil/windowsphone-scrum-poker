﻿using System.Collections.ObjectModel;
using ScrumPoker.Common;
using ScrumPoker.Models;

namespace ScrumPoker.ViewModels
{
	public class HomePageViewModel : ObservableObject
	{
		private ObservableCollection<Deck> _decks;
		public ObservableCollection<Deck> Decks
		{
			get { return _decks ?? (_decks = new ObservableCollection<Deck>()); }
			set { _decks = value; }
		}

		// TODO think how to move Frame navigation to ViewModel
	}
}
