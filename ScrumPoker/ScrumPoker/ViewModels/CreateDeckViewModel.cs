﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ScrumPoker.Common;
using ScrumPoker.Models;

namespace ScrumPoker.ViewModels
{
	public class CreateDeckViewModel : ObservableObject
	{
		private Deck _deck;
		private ICommand _addCardCommand;
		private string _cardValue;

		public Deck NewDeck
		{
			//If value isnt set assign name to value
			get
			{
				if (_deck == null)
					_deck = new Deck() {UniqueId = GetTimestamp(DateTime.Now)};
				if (String.IsNullOrEmpty(_deck.Value)) 
					_deck.Value=_deck.Name;
				return _deck;
			}
			set { _deck = value; }
		}

		public ICommand AddCardCommand
		{
			get { return _addCardCommand ?? (_addCardCommand = new RelayCommand(AddCard, ValideCardValue)); }
		}

		public string CardValue
		{
			get { return _cardValue; }
			set
			{
				if (value == _cardValue) return;
				_cardValue = value;
				OnPropertyChanged("ProductId");
				((RelayCommand)_addCardCommand).RaiseCanExecuteChanged();
			}
		}

		public void RemoveCard(Card card)
		{
			NewDeck.Cards.Remove(card);
		}

		private void AddCard()
		{
			var card = new Card()
			{
				Id = GetTimestamp(DateTime.Now),
				Value = CardValue
			};

			CardValue = String.Empty;
			OnPropertyChanged("CardValue");

			if (NewDeck.Cards == null)
				NewDeck.Cards = new ObservableCollection<Card>();
			NewDeck.Cards.Add(card);

			OnPropertyChanged("NewDeck");
		}

		private bool ValideCardValue()
		{
			return !String.IsNullOrWhiteSpace(CardValue);
		}

		private String GetTimestamp(DateTime data)
		{
			return data.ToString("yyyyMMddHHmmssffff");
		}
	}
}
